package magnileve.chungamod.tasks;

import java.util.Arrays;
import java.util.function.BooleanSupplier;

import magnileve.chungamod.Tick;
import magnileve.chungamod.TickListener;

public class Tasks {

private Tasks() {}

public static Task of(Task... tasks) {
	return new TaskList(Arrays.asList(tasks));
}

public static Task wrap(Task task, Runnable completionCallback) {
	return new TaskWrapper(task) {
		@Override
		public void start(Runnable completionCallbackWrapped) {
			super.start(() -> {
				if(completionCallback != null) completionCallback.run();
				completionCallbackWrapped.run();
			});
		}
	};
}

public static Task wrap(Task task, Runnable completionCallback, Runnable cancel, Runnable disconnect) {
	return new TaskWrapper(task) {
		@Override
		public void start(Runnable completionCallbackWrapped) {
			super.start(() -> {
				if(completionCallback != null) completionCallback.run();
				completionCallbackWrapped.run();
			});
		}
		
		@Override
		public void cancel() {
			if(cancel != null) cancel.run();
			super.cancel();
		}
		
		@Override
		public void disconnect() {
			if(disconnect != null) disconnect.run();
			super.disconnect();
		}
		
		@Override
		public boolean isActive() {
			return super.isActive();
		}
	};
}

public static Task conditional(BooleanSupplier condition) {
	return new ConditionalTask() {
		@Override
		protected boolean checkCondition() {
			return condition.getAsBoolean();
		}
	};
}

public static Task conditional(BooleanSupplier condition, Runnable start, Runnable whenFinished, Runnable onCancel, Runnable onDisconnect) {
	return new ConditionalTask() {
		@Override
		protected void started() {
			if(start != null) start.run();
			super.started();
		}
		
		@Override
		protected boolean checkCondition() {
			return condition.getAsBoolean();
		}
		
		@Override
		protected void finish() {
			if(whenFinished != null) whenFinished.run();
			super.finish();
		}
		
		@Override
		protected void cancelled() {
			if(onCancel != null) onCancel.run();
			super.cancelled();
		}
		
		@Override
		protected void disconnected() {
			if(onDisconnect != null) onDisconnect.run();
			super.disconnected();
		}
	};
}

public static Task run(Runnable run) {
	return new Task() {
		private boolean active;
		
		@Override
		public void start(Runnable completionCallback) {
			active = true;
			run.run();
			if(active) {
				active = false;
				if(completionCallback != null) completionCallback.run();
			}
		}
		
		@Override
		public boolean isActive() {
			return active;
		}
		
		@Override public void cancel() {active = false;}
		@Override public void disconnect() {active = false;}
	};
}

public static Task wait(int ticks) {
	return wait(ticks, Tick.MAIN);
}

public static Task wait(int ticks, Tick tickType) {
	return new AbstractTask() {
		TickListener listener = () -> {
			finish();
			return -1;
		};
		
		@Override
		protected void started() {
			tickType.add(listener, ticks);
		}
		
		@Override
		protected void cancelled() {
			tickType.remove(listener);
		}
	};
}

}