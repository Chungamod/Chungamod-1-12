package magnileve.chungamod.tasks;

import magnileve.chungamod.Tick;

public abstract class ConditionalTask extends TickListenerTask {

public ConditionalTask(Tick tickType) {
	super(tickType);
}

public ConditionalTask() {
	super();
}

protected abstract boolean checkCondition();

@Override
public int onTick() {
	return checkCondition() ? -1 : 1;
}

}