package magnileve.chungamod.tasks;

public class TaskWrapper implements Task {

private final Task task;

public TaskWrapper(Task task) {
	this.task = task;
}

@Override
public void start(Runnable completionCallback) {
	task.start(completionCallback);
}

@Override
public void cancel() {
	task.cancel();
}

@Override
public void disconnect() {
	task.disconnect();
}

@Override
public boolean isActive() {
	return task.isActive();
}

}