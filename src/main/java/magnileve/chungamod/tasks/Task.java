package magnileve.chungamod.tasks;

public interface Task {

public void start(Runnable completionCallback);

public void cancel();

public void disconnect();

public default void start() {
	start(null);
}

public boolean isActive();

}