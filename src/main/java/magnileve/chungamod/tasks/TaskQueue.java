package magnileve.chungamod.tasks;

import java.util.Iterator;
import java.util.Queue;

public class TaskQueue extends AbstractTask {

protected final Queue<Task> queue;

private Task current;

public TaskQueue(Queue<Task> queue) {
	this.queue = queue;
}

@Override
protected void started() {
	nextTask();
}

private void nextTask() {
	current = queue.poll();
	if(current == null) finish();
	else current.start(() -> nextTask());
}

@Override
protected void cancelled() {
	current.cancel();
	current = null;
}

@Override
protected void disconnected() {
	current.disconnect();
	current = null;
}

@Override
public String toString() {
	if(queue.isEmpty()) return "TaskQueue: []";
	Iterator<Task> iter = queue.iterator();
	StringBuilder str = new StringBuilder("TaskQueue: [").append(iter.next());
	while(iter.hasNext()) str.append(", ").append(iter.next());
	return str.append(']').toString();
}

}