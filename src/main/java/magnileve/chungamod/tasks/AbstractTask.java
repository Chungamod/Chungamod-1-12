package magnileve.chungamod.tasks;

public abstract class AbstractTask implements Task {

private static final Runnable EMPTY_CALLBACK = () -> {};

private Runnable completionCallback;

protected abstract void started();

protected abstract void cancelled();

protected void disconnected() {}

@Override
public void start(Runnable completionCallback) {
	if(isActive()) throw new IllegalStateException("Task has already been started");
	this.completionCallback = completionCallback == null ? EMPTY_CALLBACK : completionCallback;
	started();
}

@Override
public void cancel() {
	if(!isActive()) throw new IllegalStateException("Task has not been started");
	completionCallback = null;
	cancelled();
}

@Override
public void disconnect() {
	if(!isActive()) throw new IllegalStateException("Task has not been started");
	completionCallback = null;
	disconnected();
}

@Override
public boolean isActive() {
	return completionCallback != null;
}

protected void finish() {
	if(!isActive()) throw new IllegalStateException("Task has not been started");
	Runnable run = completionCallback;
	completionCallback = null;
	run.run();
}

}